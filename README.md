# Jenkins and Postgres Stack

## Description

This is Docker stack example includes sipirsipirmin/jenkinsforquery2pg and sipirsipirmin/pg_with_sample_db images. 

### sipirsipirmin/jenkinsforquery2pg 

Image extended from Jenkins's official image. Added postgresql-client and initially installed Jenkins.

Also jenkinsforquery2pg have a job named "query2pg". query2pg requests an query and sends this query to postgres(pg_with_sample_db) with using  psql. Gets result and prints.

Url for jenkinsforquery2pg: https://cloud.docker.com/repository/docker/sipirsipirmin/jenkinsforquery2pg.

### sipirsipirmin/pg_with_sample_db

Image extended from Postgresql's official image.

pg_with_sample_db's url: https://cloud.docker.com/repository/docker/sipirsipirmin/pg_with_sample_db

pg_with_sample_db have initial postgres database(named as "postgres"). When image builds, it gets sample table from https://gitlab.com/sipirsipirmin/todevops/raw/master/initialdb.sql 

## Run

Get this repo and just run `docker-compose up`

## Initial Values

Jenkins url: is http://localhost:8080

Jenkins's username: admin

Jenkins's password: fikibok

Postgres have an table named "users". You can get users with `select * from users`
